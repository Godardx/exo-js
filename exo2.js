let number1 = parseInt("2", 10);
let number2 = parseInt("4", 10);

// console.log((number1 + number2).toString());
// console.log(`${number1 + number2}`);

function add(numberToAdd) {
    console.log('value of "numberToAdd": ' + numberToAdd);

    const numbers = numberToAdd.split(',');
    console.log('value of "numbers": ' + numbers);

    if (numberToAdd === '') {
        return '0';
    }
    else {
        if (numbers.length > 1) {
            const numberReadyToAdd = [];
            let addResult = 0;

            for (let i = 0; i < numbers.length; i = i + 1) {
                numberReadyToAdd.push(parseInt(numbers[i], 10));
                addResult = addResult + numberReadyToAdd[i];
            }

            return addResult;
        }
        else {
            return numberToAdd;
        }
    }
}

const result = add('1,2');

console.log('result : ' + result);