const add = (params) => {
    if (params === '') {
        return 0;
    }
    else {
        const numbers = params.split(',');
        let somme = 0;
        numbers.forEach(element => {
            somme += parseFloat(element);
        })
        return (somme);
    }
}

console.log(add('1.1,1.2,4'));